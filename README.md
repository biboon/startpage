# Startpage

Simple HTML page intended to be used as your browser's startpage.

![alt text](preview.jpg "Preview")

## How to setup a custom page for new tabs in Firefox

[Source](https://www.reddit.com/r/startpages/comments/g3qndt/psa_how_to_set_a_custom_new_tab_page_in_firefox/)

- Open the Firefox installation folder, Windows : `C:\Program Files\Mozilla Firefox`, Linux : `/usr/lib64/firefox/` (use `whereis firefox` to find yours)

- Under this folder, open the `defaults\pref` folder. If not present, make those two folders.

- Create the file `autoconfig.js` inside the `defaults\pref` folder, and put the following in it. This lets us run our own JavaScript to configure the browser. The comment line at the top is required. Use Unix line endings.

```js
//
pref("general.config.filename", "autoconfig.cfg");
pref("general.config.obscure_value", 0);
pref("general.config.sandbox_enabled", false);
```

- Go back to the Firefox program folder

- Create the file `autoconfig.cfg`

- Put the following in the file, replacing the value of newTabURL with the path to your startpage.

```js
// first line is a comment
var {classes:Cc,interfaces:Ci,utils:Cu} = Components; k var newTabURL = "file:///PATH_TO_YOUR_START_PAGE.html";
aboutNewTabService = Cc["@mozilla.org/browser/aboutnewtab-service;1"].getService(Ci.nsIAboutNewTabService);
aboutNewTabService.newTabURL = newTabURL;
```

If you are using Firefox 76, use this instead:

```js
//
var {classes:Cc,interfaces:Ci,utils:Cu} = Components;

/* set new tab page */
try {
  Cu.import("resource:///modules/AboutNewTab.jsm");
  var newTabURL = "file:///PATH_TO_YOUR_START_PAGE.html";
  AboutNewTab.newTabURL = newTabURL;
} catch(e){Cu.reportError(e);} // report errors in the Browser Console
```
