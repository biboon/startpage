function clock(show_seconds) {
    function harold(standIn) {
        return (standIn < 10) ? "0" + standIn : standIn;
    }

    var now = new Date();
    var ret = harold(now.getHours()) + ":" + harold(now.getMinutes());

    if ( show_seconds ) {
        ret += ":" + harold(now.getSeconds());
    }

    return ret;
}

function date() {
    const days   = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
    const months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];

    var now = new Date();

    return days[now.getDay()] + ", " + months[now.getMonth()] + " " + now.getDate();
}

function image_asyncget(url, expire, callback) {
    var storage = window.localStorage;

    var now  = Date.now();
    var then = parseInt(storage.getItem("img_timestamp"), 10);

    if ( isNaN(then) || (now - then) > (expire * 1000) ) {
        var xhr = new XMLHttpRequest();

        xhr.onload = function() {
            var data        = new Uint8Array(xhr.response);
            var data_as_str = new String();

            var len = 4096;
            for ( var off = 0; off < data.length; off += len )
            {
                if ( len > (data.length - off) ) len = data.length - off;
                var chunk = data.slice(off, off + len);
                data_as_str += String.fromCharCode(...chunk);
            }

            var b64 = btoa(data_as_str);

            storage.setItem("img_timestamp", Date.now());
            storage.setItem("img_data", b64);
            storage.setItem("img_url", xhr.responseURL);

            console.log("Image loaded");
            callback(b64, xhr.responseURL);
        };

        xhr.responseType = "arraybuffer";
        xhr.open("GET", url);
        xhr.send();
    } else {
        var b64 = storage.getItem("img_data");
        var url = storage.getItem("img_url");
        console.log("Image read");
        callback(b64, url);
    }
}
